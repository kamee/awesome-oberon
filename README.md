 a curated list of awesome oberon papers and modules.

# papers, articles

[Modula-2 and Oberon. by Niklaus Wirth](https://oberoncore.ru/_media/library/n._wirth_-_modula-2_and_oberon_hopl_iii_.pdf)

[FFF97 – Oberon in the Real World. Dr. Josef Templ](http://norayr.am/papers/fff97.pdf)

[A Brief History of Modula-2. K. N. King](https://kamee.gitlab.io/tech/notes/an-overview/)

[History and Goals of Modula-2 By Niklaus Wirth](https://www.drdobbs.com/open-source/history-and-goals-of-modula-2/223000094)

[What's New With Modula-2? By K.N. King](https://www.drdobbs.com/whats-new-with-modula-2/184408565)

[Oberon vs. C++ by Josef Templ](http://www.modulaware.com/mdlt49.htm)

[Comparing Modula-2 and C++](https://www.drdobbs.com/cpp/comparing-modula-2-and-c/184408071)

[Programming in Modula-2(An Introduction to Modula-2) BY CAROLYN ROGERS](https://www.atarimagazines.com/startv4n2/modula2.html)

[Modula-2 User Manual](http://modula2.awiedemann.de/manual/comp2.html)

[Oberon-2, a hi-performance alternative to C++](https://folk.ntnu.no/haugwarb/Programming/Oberon/oberon_vs_cpp_II.htm)

[Implementing call-by-reference and call-by-name in Lua. by Rochus Keller](https://medium.com/@rochus.keller/implementing-call-by-reference-and-call-by-name-in-lua-47b9d1003cc2)

[Less is more. Why Oberon beats mainstream in complex applications](https://iopscience.iop.org/article/10.1088/1742-6596/523/1/012011/pdf)

[Oberon – The Overlooked Jewel by Michael Franz](https://pdfs.semanticscholar.org/d48b/ecdaf5c3d962e2778f804e8c64d292de408b.pdf)

[Modula-3 (article reprinted from USENIX)](http://www.redbarn.org/node/20)

[On Programming Styles bv Niklaus Wirth](https://people.inf.ethz.ch/wirth/Miscellaneous/Styles.pdf)

[A Description of the Oberon-2 Language. by Paul Floyd ](http://www.edm2.com/0608/oberon2.html)

[A Discussion of Oberon. by Paul Floyd ](http://www.edm2.com/0608/oberon.html)

[Programming Without Enumerations in Oberon by C. Lins](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.97.7983&rep=rep1&type=pdf)

# system

[Active Object System (AOS aka A2)](http://cas.inf.ethz.ch/projects/a2)

[Oberon Computing Systems. Wikibook](https://en.wikibooks.org/wiki/Oberon)

[ULM Oberon System](http://www.mathematik.uni-ulm.de/oberon/)

[Pepino Oberon](https://www.saanlima.com/pepino/index.php?title=Pepino_Oberon)

# compiler

[voc](https://github.com/vishaps/voc)

[Oberon 07 compiler](https://github.com/vladfolts/oberonjs)

[MultiOberon Compiler](https://github.com/dvdagaev/Mob)

[obnc](https://miasap.se/obnc/)

[GNU Modula2](ftp://ftp.gwdg.de/pub/gnu/www/savannah-checkouts/non-gnu/gm2/gm2.html)

[ModulAware](http://www.modulaware.com/)

# fun, programs, resources

[xenopusRTRT](https://www.youtube.com/user/xenopusRTRT/videos)

[modula 2](http://www.nezumi.demon.co.uk/modula/m2.htm)

[odcread - read ".odc" oberon compound documents](https://github.com/gertvv/odcread)

[Karax](https://github.com/kekcleader/Karax)

[Каталог интернет-ресурсов, связанных с языком программирования Оберон](https://oberon.org/)

[Oberon-generic-heap-allocation](https://github.com/andreaspirklbauer/Oberon-generic-heap-allocation)

[Gardens Point Component Pascal (GPCP)](https://github.com/k-john-gough/gpcp)

[BlackBox Component Builder for Windows, GNU/Linux, OpenBSD, FreeBSD](https://github.com/bbcb/bbcp)

[BlackBox Framework](https://blackboxframework.org/index.php?cID=home,en-us)

[Go Oberon OpenGL](https://github.com/kekcleader/goAndOberonOpenGL)

[Ulm's Oberon Library: Alphabetical Module Index](http://www.mathematik.uni-ulm.de/oberon/0.5/lib/man/)

[Ulm's Modula-2 System](http://www.mathematik.uni-ulm.de/modula/)

[An Oberon development system for NXP ARM Cortex-M3 Microcontrollers](https://www.astrobe.com/Oberon.htm)

[Cetus Links: Object-Oriented Language: Oberon-2 / Component Pascal](http://archive.adaic.com/ase/ase02_01/bookcase/ref_sh/cetusweb/oo_oberon.html)

[Oberon Core(russian)](https://oberoncore.ru/)

[Component Pascal Collection](http://zinnamturm.eu/)

[Oberon Language Genealogy Tree](https://web.archive.org/web/20191207132227/http://www.ethoberon.ethz.ch/genealogy.html)

[RISK5](http://www.riskfive.com/)

[The Oberon-2 language and environment](https://researchprofiles.herts.ac.uk/portal/en/publications/the-oberon2-language-and-environment(5b40b10b-a8cf-4d38-b9dc-f666a586c75a).html)


# books

[Modula-2: A Complete Guide (College)](https://b-ok.cc/book/2701214/e515c0) by Kimberly Nelson King

[Programming in Oberon](https://inf.ethz.ch/personal/wirth/Oberon/PIO.pdf) by Niklaus Wirth

[Into the Realm of Oberon: An Introduction to Programming and the Oberon-2 Programming Language](https://b-ok.cc/book/2098185/4afae3) by Eric W. Nikitin

[Programming in Oberon: Steps Beyond Pascal and Modula](https://b-ok.cc/book/679869/611b48) by Martin Reiser, Niklaus Wirth

[A Guide to Modula-2](https://b-ok.cc/book/2116686/64b0ff) by Kaare Christian

[HOW TO PROGRAM A COMPUTER (Using the oo2c Oberon-2 Compiler)](http://www.waltzballs.org/other/prog.html) by Donald Daniel

[Programming in Modula-2](https://b-ok.cc/book/2116360/f0c791)  by Niklaus Wirth

[Object-Oriented Programming: in Oberon-2](https://b-ok.cc/book/2118005/1f5cfa) by  Hanspeter Mössenböck

[Oberon-2 Programming with Windows [With Full Windwos Based Integrated Development]](https://b-ok.cc/book/2098364/b630fb) by  by Jörg Mühlbacher, B. Kirk, U. Kreuzeder

[The Programming Language Oberon-2, by H. Mössenböck, N. Wirth](https://cseweb.ucsd.edu/~wgg/CSE131B/oberon2.htm)

[Oberon07 Report. Niklaus Wirth ](https://people.inf.ethz.ch/wirth/Oberon/Oberon07.Report.pdf)

[Modula-2: Abstractions for Data and Programming Structures (Using ISO-Standard Modula-2) by Richard J. Sutcliffe](http://www.csc.twu.ca/rsbook/contents.html)


# IDEs, Editors, and Plugins

[Free Oberon](https://freeoberon.su/en/) - A cross-platform IDE for development in Oberon programming language made in the classical FreePascal-like pseudo-graphic style.

# emulator

[Oberon RISC Emulator for Pascal](https://github.com/MGreim/riscpas_repo)

[Oberon Emulator](https://schierlm.github.io/OberonEmulator/emu.html?image=ColorDiskImage&width=1024&height=576)

# forum, discussions, blogs

[Oberon Operating System. HN](https://news.ycombinator.com/item?id=21383016)

[Oberon / BlackBox for REAL projects?](http://computer-programming-forum.com/28-oberon/aab447a99186a073.htm)

[Oberon List](https://lists.inf.ethz.ch/pipermail/oberon/2020/date.html#13797)

[FAQ on the Oberon Language](https://web.archive.org/web/20190831185610/http://www.ethoberon.ethz.ch/faqlang.html#name)

[Questions tagged [oberon]](https://stackoverflow.com/questions/tagged/oberon)

[Oberon resources for Linux users and developers](https://static.lwn.net/lwn/1998/0507/a/oberon.html)

[modula 2](https://www.modula2.org/modula-2.php)

[The Enum is Dead. Long Live the Enum](http://computer-programming-forum.com/28-oberon/34d42e6f0943d4b0.htm)

[github: topics/oberon](https://github.com/topics/oberon)

[BlackBox Framework Center: Forum](https://forum.blackboxframework.org/)

[OberonScript: a safe scripting language and runtime for web apps (2007)](https://news.ycombinator.com/item?id=24909114&ref=hvper.com)

[Oberon / BlackBox for REAL projects ?](http://computer-programming-forum.com/28-oberon/aab447a99186a073.htm)

# videos

[Программирование на Обероне/Oberon Programming](https://www.youtube.com/channel/UCfaaot6JjktvqQCUOkXxEsA)

[Oberon (Native Oberon 2.3.7). Systems with JT](https://youtu.be/OJGnpmnXR5w)

[OberonCore](https://www.youtube.com/channel/UCqunWQWndKQPyF0ncD7F7Ew/featured)

# wirth

[Niklaus Wirth: Geek of the Week](https://www.red-gate.com/simple-talk/opinion/geek-of-the-week/niklaus-wirth-geek-of-the-week/)

[Niklaus Wirth - a Pioneer of Computer Science.](https://www.researchgate.net/publication/221350629_Niklaus_Wirth_-_a_Pioneer_of_Computer_Science)

[Niklau Wirth. 1984 ACM A.M. Turing Award Recipient](http://www-oldurls.inf.ethz.ch/personal/wirth/Articles/TuringAward.pdf)

# people, project


[norayr](https://github.com/norayr)

[Артур Ефимов](https://github.com/kekcleader)

[Andreas Pirklbauer](https://github.com/andreaspirklbauer)

[hansklav /Oberon-07](https://github.com/hansklav/Oberon-07)

_Some modules I wrote in or ported to Niklaus Wirth's Oberon-07 programming language._

[fabwu/syscon](https://github.com/fabwu/syscon)

_System Construction Lecture at ETH which introduces and illustrates principles of computer system construction with four case studies._

[eterps/Juice](https://github.com/eterps/Juice)

_Juice - Oberon answer to Java browser plugins (1997)_

[Project Norebo](https://github.com/pdewacht/project-norebo)

_Norebo is a hack to run some Project Oberon 2013 software on the Unix command line. Programs that use the GUI obviously won't work, but e.g. the 
compiler runs._

[BlackBox Framework Center](https://github.com/BlackBoxCenter)

[Fmt](https://github.com/rsdoiel/fmt)
_An experimental Oberon-7 module to provide format functions for INTEGER and REAL values. It is inspired by Karl Landström's OBNC oberon compiler and his extension library._

[Astrobe for FPGA RISC5 v7.2. Embedded Project Oberon](https://www.astrobe.com/RISC5/)

[Ralph Sommerer](http://www.ralphsommerer.com/obn.htm)

[Visual](https://visual.sfu-kras.ru/)
_Visual is a tool for creating interactive educational and scientific models. The project aims to disseminate knowledge and teach programming._

_The code for simulations is written in the simple and reliable programming language Oberon in combination with the OberonJS translator and the p5.js framework for HTML5 canvas drawing._
